package app.instantanswertarot.com;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ProgressBar;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.quantcast.measurement.service.QuantcastClient;
import com.yieldmo.sdk.Yieldmo;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public abstract class Activity_Base extends Activity {

    private static final String LOG_EVENT_CATEGORY = "category";
    private static final String LOG_EVENT_ACTION = "action";
    private static final String LOG_EVENT_LABEL = "label";
    private static final String LOG_EVENT_VALUE = "value";

    static final Intent tarotIntent = new Intent("CardDescription");
    static final Intent HomeIntent = new Intent("Home");
    static final Intent BrowseIntent = new Intent("BrowseCard");
    static final Intent ChangeDeckIntent = new Intent("ChangeDeck");
    static final Intent InfoIntent = new Intent("Info");
    static final Intent PickACardIntent = new Intent("PickACard");
    static final Intent ResultPageIntent = new Intent("ResultContent");
    static final Intent ExtraIntent = new Intent("Extra");
    static final Intent webIntent = new Intent(Intent.ACTION_VIEW);

    public static Calendar calendar = Calendar.getInstance();
    private FirebaseAnalytics firebaseAnalytics;

    public abstract int getLayout();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        //getWindow().setWindowAnimations(R.anim.fade);
        //YMSdk.init(this, Const.isDebug? YMConstants.testAppId : "4d4a721f-7177-45e6-b309-3a3867d1fb75", Const.isDebug? true : false);
        Yieldmo.initialize("4d4a721f-7177-45e6-b309-3a3867d1fb75", getApplicationContext());
        calendar.setTime(new Date());
        setContentView(getLayout());
        showBannerAds();

        Utils.interstitial.init(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        QuantcastClient.activityStart(this, Const.QuantcastKey, null, null);
        QuantcastClient.enableLogging(BuildConfig.DEBUG);
        new FlurryAgent.Builder().build(this, Const.FlurryKey);
        trackPages();
    }

    @Override
    public void onStop() {
        super.onStop();
        QuantcastClient.activityStop();
        FlurryAgent.onEndSession(this);
    }

    public void trackPages() {
        String trackingTag = getTrackingTag();
        if (trackingTag != null) {
            FlurryAgent.logEvent(trackingTag);
            trackScreen(trackingTag.replaceAll("\\s+", ""));
            fireBaseLogEventScreen(trackingTag.replaceAll("\\s+", ""));
        }
    }

    public void showBannerAds() {

    }

    public String getTrackingTag() {
        return null;
    }

    public void hide(int id) {
        hide(findViewById(id));
    }

    public void hide(View view) {
        view.setVisibility(View.INVISIBLE);
    }

    public void remove(int id) {
        remove(findViewById(id));
    }

    public void remove(View view) {
        view.setVisibility(View.GONE);
    }

    public void show(int id) {
        show(findViewById(id));
    }

    public void show(View view) {
        view.setVisibility(View.VISIBLE);
    }

    public ProgressDialog loadingMessage;

    public void showLoading() {
        if (loadingMessage == null || loadingMessage.getContext() != this) {
            loadingMessage = new ProgressDialog(this);
        }

        if (!loadingMessage.isShowing() && !isFinishing()) {
            try {
                loadingMessage.show();
                loadingMessage.setContentView(new ProgressBar(this)); //, null, android.R.attr.progressBarStyleLarge));
                loadingMessage.setCancelable(true);
            } catch (Exception e) {
                loadingMessage = null;
                e.printStackTrace();
            }
        }
    }

    public void hideLoading() {
        if (loadingMessage != null)
            try {
                loadingMessage.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }

        loadingMessage = null;
    }

    public void setBottomMenu(String ls_page) {
        findViewById(R.id.home).setOnClickListener(bottomMenuClickListener);
        findViewById(R.id.browse).setOnClickListener(bottomMenuClickListener);
        findViewById(R.id.changedeck).setOnClickListener(bottomMenuClickListener);
        findViewById(R.id.info).setOnClickListener(bottomMenuClickListener);
        findViewById(R.id.extra).setOnClickListener(bottomMenuClickListener);
        if (ls_page.length() > 0) {
            findViewById(getID(ls_page, "id")).setBackgroundResource(getID("tab_" + ls_page + "_sel", "drawable"));
        }
    }

    public OnClickListener bottomMenuClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int button_id = v.getId();
            if (button_id == R.id.browse) {
                Utils.interstitial.display(BrowseIntent);
            } else if (button_id == R.id.changedeck) {
                Utils.interstitial.display(ChangeDeckIntent);
            } else if (button_id == R.id.info) {
                Utils.interstitial.display(InfoIntent);
            } else if (button_id == R.id.extra) {
                Utils.interstitial.display(ExtraIntent);
            } else {
                Utils.interstitial.display(HomeIntent);
            }
        }
    };

    public String getCardTitle(int cardIndex) {
        return getString(getID("card_title_" + (cardIndex + 1), "string"));
    }

    public int getFaceCardId(int cardIndex) {
        return getID("card_" + getUserPreference("carddeck", "a") + "_" + (cardIndex + 1), "drawable");
    }

    public int getBackground(Const.QUESTION_TYPE type) {
        return getID("bg_" + type.toString().toLowerCase(Locale.US), "drawable");
    }

    public String getUserPreference(String key, String defaultValue) {
        return getSharedPreferences(Const.APP_PREF, 0).getString(key, defaultValue);
    }

    public void setUserPreference(String key, String value) {
        getSharedPreferences(Const.APP_PREF, 0).edit().putString(key, value).apply();
    }

    public int getID(String l_idString, String l_type) {
        return Utils.getID(this, l_idString, l_type);
    }

    public void loadYielmoAd(int webViewId, String adScript) {
        Utils.showAd(this, webViewId, adScript);
    }

    public void trackScreen(String screenName) {
        YesOrNoTarotApplication application = (YesOrNoTarotApplication) getApplication();
        Tracker tracker = application.getDefaultTracker();
        tracker.setScreenName(screenName);
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    public void trackEvent(String category, String action, String label, int value) {
        YesOrNoTarotApplication application = (YesOrNoTarotApplication) getApplication();
        Tracker tracker = application.getDefaultTracker();
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .setLabel(label)
                .setValue(value)
                .build());
    }

    public void fireBaseLogEventScreen(String screenName) {
        if (firebaseAnalytics != null) {
            firebaseAnalytics.logEvent(screenName, null);
        } else {
            firebaseAnalytics = FirebaseAnalytics.getInstance(this);
            firebaseAnalytics.logEvent(screenName, null);
        }
    }

    public void fireBaseLogEvent(String category, String action, String label, int value) {
        Bundle params = new Bundle();
        params.putString(LOG_EVENT_CATEGORY, category);
        params.putString(LOG_EVENT_ACTION, action);
        params.putString(LOG_EVENT_LABEL, label);
        params.putInt(LOG_EVENT_VALUE, value);
        firebaseAnalytics.logEvent(action, params);
    }
}