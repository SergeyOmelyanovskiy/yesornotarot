package app.instantanswertarot.com;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Activity_BrowseCard extends Activity_Base implements OnItemClickListener {

    public int getLayout() {
        return R.layout.screen_browsecard;
    }

    public String getTrackingTag() {
        return "Browse Card Main";
    }

    public void showBannerAds() {
        loadYielmoAd(R.id.ad, "yieldmoBottom.html");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ListView listview = findViewById(R.id.cardlist);
        listview.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.card_titles)));
        listview.setOnItemClickListener(this);
        setBottomMenu("browse");
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        tarotIntent.putExtra("cardnumber", position);
        tarotIntent.putExtra("swipe", false);
        trackEvent("CardList", "Clicked", "cardnumber" + position, 0);
        fireBaseLogEvent("CardList", "Clicked", "cardnumber" + position, 0);
        Utils.interstitial.display(tarotIntent);
    }
}