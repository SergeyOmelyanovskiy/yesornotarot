package app.instantanswertarot.com;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;

public class Activity_CardDescription extends Activity_Base implements OnClickListener {

    private LinearLayout cardDescription, zoom;
    private ViewGroup mContainer;
    private ImageView mCardZoomedIn, mCardZoomedOut, ZoomedInLeftArrow, ZoomedInRightArrow;
    private TextView cardLabel;

    private Animation fadeIn, fadeOut, anim, anim2, slide_in_right, slide_in_left, slide_out_right, slide_out_left = null;
    private int cardNumber, cardImageId, cardTextId;
    private String cardTitle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initAnim();
        initLayout();
        setBottomMenu("browse");
    }

    public void zoomOut() {
        anim2.setDuration(1100);
        mCardZoomedIn.setAnimation(anim2);
        ZoomedInRightArrow.startAnimation(fadeOut);
        ZoomedInLeftArrow.startAnimation(fadeOut);
        cardLabel.startAnimation(fadeOut);
        remove(ZoomedInRightArrow);
        remove(ZoomedInLeftArrow);
        remove(mCardZoomedIn);
        remove(cardLabel);

        Utils.setTimer(this, "showZoomOut", 900);
    }

    public void zoomIn() {
        remove(cardDescription);
        mContainer.setBackgroundColor(Color.BLACK);
        show(zoom);
        zoom.startAnimation(fadeIn);
        anim.setDuration(900);
        hide(ZoomedInRightArrow);
        hide(ZoomedInLeftArrow);
        mCardZoomedIn.setAnimation(anim);

        Utils.setTimer(this, "showZoomIn", 900);
    }

    public void goToNextCard(int currentClickId) {
        int previousCard = cardNumber <= 0 ? 21 : cardNumber - 1;
        int nextCard = cardNumber >= 21 ? 0 : cardNumber + 1;
        boolean go_right = currentClickId == R.id.nextcard || currentClickId == R.id.right;
        boolean zoomInView = currentClickId == R.id.right || currentClickId == R.id.left;
        tarotIntent.putExtra("cardnumber", go_right ? nextCard : previousCard);
        tarotIntent.putExtra("swipe", zoomInView);
        tarotIntent.putExtra("swipe_right", go_right);
        mCardZoomedIn.startAnimation(go_right ? slide_out_right : slide_out_left);
        cardLabel.startAnimation(go_right ? slide_out_right : slide_out_left);

        Utils.setTimer(this, "showNextCard", zoomInView ? 200 : 0);
    }

    public void showNextCard() {
        hide(mCardZoomedIn);
        hide(cardLabel);
        Utils.interstitial.display(tarotIntent, tarotIntent.getBooleanExtra("swipe", false));
    }

    public void showZoomIn() {
        show(cardLabel);
        show(ZoomedInRightArrow);
        show(ZoomedInLeftArrow);
        cardLabel.startAnimation(fadeIn);
        cardLabel.setText(cardTitle);
        ZoomedInRightArrow.startAnimation(fadeIn);
        ZoomedInLeftArrow.startAnimation(fadeIn);
    }

    public void showZoomOut() {
        zoom.startAnimation(fadeOut);
        tarotIntent.putExtra("cardnumber", cardNumber);
        tarotIntent.putExtra("swipe", false);
        Utils.interstitial.display(tarotIntent, true);
    }

    public void initAnim() {
        slide_out_right = AnimationUtils.loadAnimation(this, R.anim.slide_out_right);
        slide_out_left = AnimationUtils.loadAnimation(this, R.anim.slide_out_left);
        slide_in_right = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);
        slide_in_left = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);
        fadeIn = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
        fadeOut = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
        anim = AnimationUtils.loadAnimation(this, R.anim.zoom2);
        anim2 = AnimationUtils.loadAnimation(this, R.anim.zoomout);

        slide_out_right.setRepeatCount(0);
        slide_out_left.setRepeatCount(0);
        slide_in_right.setRepeatCount(0);
        slide_in_left.setRepeatCount(0);
    }

    public void initLayout() {
        Bundle extras = getIntent().getExtras();
        boolean isZoomedIn = extras.getBoolean("swipe");
        boolean swipedRight = extras.getBoolean("swipe_right");

        cardNumber = extras.getInt("cardnumber");
        cardTitle = getCardTitle(cardNumber);
        cardImageId = getFaceCardId(cardNumber);
        cardTextId = getID("card_" + (cardNumber + 1), "string");

        cardDescription = findViewById(R.id.description);
        zoom = findViewById(R.id.zoom);
        mCardZoomedIn = findViewById(R.id.zoomin);
        mCardZoomedOut = findViewById(R.id.cardimage);
        mContainer = findViewById(R.id.container);
        cardLabel = findViewById(R.id.cardlabel);
        ZoomedInRightArrow = findViewById(R.id.right);
        ZoomedInLeftArrow = findViewById(R.id.left);
        findViewById(R.id.shadow).startAnimation(fadeIn);

        mCardZoomedOut.setImageResource(cardImageId);
        mCardZoomedIn.setImageResource(cardImageId);
        ((TextView) findViewById(R.id.cardname)).setText(cardTitle);
        ((TextView) findViewById(R.id.carddescription)).setText(cardTextId);

        if (isZoomedIn) {
            remove(cardDescription);
            mContainer.setBackgroundColor(Color.BLACK);
            mCardZoomedIn.startAnimation(swipedRight ? slide_in_left : slide_in_right);
            cardLabel.startAnimation(swipedRight ? slide_in_left : slide_in_right);
            show(zoom);
            cardLabel.setText(cardTitle);
        }

        mCardZoomedOut.setOnClickListener(this);
        mCardZoomedIn.setOnClickListener(this);
        ZoomedInRightArrow.setOnClickListener(this);
        ZoomedInLeftArrow.setOnClickListener(this);
        findViewById(R.id.nextcard).setOnClickListener(this);
        findViewById(R.id.previouscard).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cardimage:
                zoomIn();
                break;
            case R.id.zoomin:
                zoomOut();
                break;
            case R.id.right:
            case R.id.left:
            case R.id.nextcard:
            case R.id.previouscard:
                goToNextCard(v.getId());
                break;
            default:
                break;
        }
    }

    @Override
    public void onRestart() {
        super.onRestart();
        showBannerAds();
        show(cardDescription);
        mContainer.setBackgroundResource(R.drawable.bg_bwbullseye);
        remove(zoom);
        remove(cardLabel);
    }

    @Override
    public int getLayout() {
        return R.layout.screen_carddescription;
    }

    @Override
    public void trackPages() {
        String cardNameWithoutSpace = cardTitle.replace(" ", "_");
        String cardNameWithoutDot = cardNameWithoutSpace.replace(".", "");
        FlurryAgent.logEvent("CardDescription_" + cardNameWithoutDot);
        trackScreen("CardDescription_" + cardNameWithoutDot);
        fireBaseLogEventScreen("CardDescription_" + cardNameWithoutDot);
    }

    @Override
    public void showBannerAds() {
        loadYielmoAd(R.id.ad, "yieldmoBottom.html");
    }
}    