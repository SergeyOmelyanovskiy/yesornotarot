package app.instantanswertarot.com;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;

public class Activity_ChangeDeck extends Activity_Base implements OnClickListener {

    public int getLayout() {
        return R.layout.screen_changedeck;
    }

    public String getTrackingTag() {
        return "Change Deck Page";
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        findViewById(R.id.carda).setOnClickListener(this);
        findViewById(R.id.cardb).setOnClickListener(this);
        findViewById(R.id.cardc).setOnClickListener(this);
        setBottomMenu("changedeck");

        String cardDeck = getUserPreference("carddeckname", "1001Nights");
        switch (cardDeck) {
            case "1001Nights":
                setupTitle(R.id.cardTexta);
                break;
            case "Bruegel":
                setupTitle(R.id.cardTextb);
                break;
            case "Egyptian":
                setupTitle(R.id.cardTextc);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        String cardtype = "a";
        if (viewId == R.id.cardb) {
            cardtype = "b";
        } else if (viewId == R.id.cardc) {
            cardtype = "c";
        }

        int TextViewId = getID("cardText" + cardtype, "id");
        String cardDeckName = ((TextView) findViewById(TextViewId)).getText().toString();
        setUserPreference("carddeck", cardtype);
        setUserPreference("carddeckname", cardDeckName.replace(" ", ""));

        FlurryAgent.logEvent("Deck version picked: " + cardDeckName);
        trackEvent("Deck", "Clicked", cardDeckName.replace(" ", "_"), 0);
        fireBaseLogEvent("Deck", "Clicked", cardDeckName.replace(" ", "_"), 0);

        Toast toast = Toast.makeText(this, "Deck Selected: " + cardDeckName, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 40);
        toast.show();

    	Utils.interstitial.display(HomeIntent);
    }

    private void setupTitle(int textViewId) {
        TextView title = findViewById(textViewId);
        title.setTypeface(title.getTypeface(), Typeface.BOLD);
    }
}