package app.instantanswertarot.com;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;

@SuppressLint("SetJavaScriptEnabled")
public class Activity_Extra extends Activity_Base implements OnClickListener {

    private static String startUrl;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        hide(R.id.button_top_right);
        findViewById(R.id.button_top_left).setOnClickListener(this);

        Bundle UrlParams = new Bundle();
        UrlParams.putString("src", Const.l_markettype.toString());
        UrlParams.putString("ap", "yntarot");
        UrlParams.putString("Af", "1007");

        WebView webview = findViewById(R.id.webview);
        webview.setWebViewClient(new myWebViewClient(this));
        webview.getSettings().setJavaScriptEnabled(true);
        startUrl = getString(R.string.url_extra);
        webview.loadUrl(Utils.generateURL(Utils.getServerLink(this, startUrl), UrlParams));

        setBottomMenu("extra");
    }

    public static class myWebViewClient extends WebViewClient {
        Activity_Base mActivity;

        myWebViewClient(Activity_Base activity) {
            mActivity = activity;
        }

        @Override
        public boolean shouldOverrideUrlLoading(final WebView view, final String url) {
            Activity_Extra.startUrl = url;
            if (!Utils.isDeviceConnected(mActivity)) {
                onReceivedError(view, -2, "no internet connection", "");
            } else if (url.startsWith("market")) {
                webIntent.setData(Uri.parse(url));
                mActivity.startActivity(webIntent);
            } else
                view.loadUrl(url);

            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            if (!url.contains("index")) {
                Utils.reloadAd(mActivity, R.id.ad);
            }

            mActivity.showLoading();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            mActivity.hideLoading();
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            String errorMsg = BuildConfig.DEBUG ? (description + ": " + failingUrl) : "";
            String summary = "<html><body bgcolor=\"black\" align=\"center\"><font color=\"white\"><br><br>" + errorMsg + "<br><br>Problem loading this page.<br>Please try again later.</font></body></html>";
            view.loadData(summary, "text/html", "utf-8");
            mActivity.hideLoading();
        }
    }

    @Override
    public void onClick(View v) {
        int view_id = v.getId();
        if (view_id == R.id.button_top_left) {
            Utils.interstitial.display(HomeIntent); // startActivity(Utils.HomeIntent);
        }
    }

    @Override
    public int getLayout() {
        return R.layout.screen_extra;
    }

    @Override
    public String getTrackingTag() {
        return "Extra Page";
    }

    @Override
    public void showBannerAds() {
        loadYielmoAd(R.id.ad, "yieldmoBottom.html");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (!startUrl.contains(getString(R.string.url_extra))) {
            startActivity(ExtraIntent);
        }
    }
}