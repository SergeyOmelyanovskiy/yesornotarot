package app.instantanswertarot.com;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class Activity_Home extends Activity_Base implements OnClickListener {

    public int getLayout() {
        return R.layout.screen_home;
    }

    public String getTrackingTag() {
        return "Main Page";
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((TextView) findViewById(R.id.version)).setText(getString(R.string.app_version, Utils.getVersionNumber(this)));

        findViewById(R.id.love).setOnClickListener(this);
        findViewById(R.id.career).setOnClickListener(this);
        findViewById(R.id.money).setOnClickListener(this);
        findViewById(R.id.general).setOnClickListener(this);

        setBottomMenu("home");
    }

    @Override
    public void onClick(View v) {
        int view_id = v.getId();
        Const.QUESTION_TYPE type = null;
        if (view_id == R.id.love) {
            type = Const.QUESTION_TYPE.LOVE;
        } else if (view_id == R.id.career) {
            type = Const.QUESTION_TYPE.CAREER;
        } else if (view_id == R.id.money) {
            type = Const.QUESTION_TYPE.MONEY;
        } else if (view_id == R.id.general) {
            type = Const.QUESTION_TYPE.GENERAL;
        }

        if (type != null) {
            PickACardIntent.putExtra("type", type);
            trackEvent("QuestionType", "Clicked", type.toString(), 0);
            fireBaseLogEvent("QuestionType", "Clicked", type.toString(), 0);
            startActivity(PickACardIntent);
        }
    }
}