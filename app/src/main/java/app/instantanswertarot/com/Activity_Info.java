package app.instantanswertarot.com;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class Activity_Info extends Activity_Base implements OnClickListener {

    static final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
    static final String EMAIL_DESTINATION = android.content.Intent.EXTRA_EMAIL;
    static final String EMAIL_SUBJECT = android.content.Intent.EXTRA_SUBJECT;
    static final String EMAIL_BODY = android.content.Intent.EXTRA_TEXT;

    public int getLayout() {
        return R.layout.screen_info;
    }

    public String getTrackingTag() {
        return "Info Page";
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((TextView) findViewById(R.id.copyright_view)).setText(getString(R.string.copyright, calendar.get(Calendar.YEAR)));
        ((TextView) findViewById(R.id.version)).setText(getString(R.string.app_version, Utils.getVersionNumber(this)));

        findViewById(R.id.feedback).setOnClickListener(this);
        findViewById(R.id.rate).setOnClickListener(this);
        findViewById(R.id.suggest).setOnClickListener(this);

        setBottomMenu("info");
    }

    @Override
    public void onClick(View v) {
        int view_id = v.getId();
        if (view_id == R.id.rate) {
            goToWeb(Utils.getMarketLink(this));
        } else if (view_id == R.id.suggest) {
            emailFriend("Yes Or No Tarot app", "Check out this app: " + Utils.getMarketLink(this), this);
        } else if (view_id == R.id.feedback) {
            Bundle UrlParams = new Bundle();
            UrlParams.putString("src", "androidYNT" + Utils.getVersionNumber(this).replace('.', '_'));
            UrlParams.putString("Af", "1007");
            goToWeb(Utils.generateURL(Utils.getServerLink(this, getString(R.string.url_feedback)), UrlParams));
        }
    }

    public void goToWeb(final String url) {
        new AlertDialog.Builder(this)
                .setTitle("You will be leaving this application by clicking OK")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int whichButton) {
                        webIntent.setData(Uri.parse(url));
                        startActivity(webIntent);
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();
    }

    public static void emailFriend(String subject, String text, Activity a) {
        emailIntent.setType("plain/text");
        emailIntent.putExtra(EMAIL_DESTINATION, new String[]{""});
        emailIntent.putExtra(EMAIL_SUBJECT, subject);
        emailIntent.putExtra(EMAIL_BODY, text);
        a.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }
}