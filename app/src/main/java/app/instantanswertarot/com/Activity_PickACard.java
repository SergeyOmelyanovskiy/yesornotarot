package app.instantanswertarot.com;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import app.instantanswertarot.com.Const.QUESTION_TYPE;

import com.example.flip3d.DisplayNextView;
import com.example.flip3d.Flip3dAnimation;
import com.flurry.android.FlurryAgent;

public class Activity_PickACard extends Activity_Base {

    private ImageView faceCard;
    private TextView cardLabel;
    private int cardIndex, selectedCardImageId;
    private RelativeLayout cards;
    private LinearLayout container, instruction;
    private Animation fadeOut, fadeIn, anim2;
    private final int NUM_OF_CARD_SPREAD = 11;
    private int containerWidth; //, containerheight;
    private Const.QUESTION_TYPE type;

    private final ScaleAnimation none = new ScaleAnimation(1, 1, 1, 1);
    private final ScaleAnimation scale = new ScaleAnimation(1f, 1.52f, 1f, 1.52f, ScaleAnimation.RELATIVE_TO_SELF, .5f, ScaleAnimation.RELATIVE_TO_SELF, .5f);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initAnim();
        initLayout();
        setCards();
        setBottomMenu("home");
    }

    public void initLayout() {
        Bundle extras = getIntent().getExtras();
        type = (QUESTION_TYPE) extras.get("type");

        container = findViewById(R.id.container);
        cards = findViewById(R.id.cards);
        cards.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_slide_right));
        cardLabel = findViewById(R.id.cardlabel);
        instruction = findViewById(R.id.instruction);
        faceCard = findViewById(R.id.facecard);
        container.setBackgroundResource(getBackground(type));
    }

    public void initAnim() {
        fadeOut = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
        fadeIn = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
        anim2 = AnimationUtils.loadAnimation(this, R.anim.zoomoutv2);
    }

    public OnClickListener aCardOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            showSelectedCard((ImageView) view);
        }
    };

    public void showSelectedCard(ImageView selectedCard) {
        hideAllCards();

        //containerheight = cards.getHeight();
        containerWidth = cards.getWidth();
        cardIndex = Utils.randomGenerator.nextInt(22);
        selectedCardImageId = getFaceCardId(cardIndex);

        faceCard.setImageResource(selectedCardImageId);
        cards.setBackgroundColor(Color.BLACK);
        cards.setAnimation(fadeIn);
        remove(instruction);
        container.setBackgroundColor(Color.BLACK);

        applyRotation(true, selectedCard, faceCard, 0f, 90f);

        Utils.setTimer(this, "showCardText", 1100);
        Utils.setTimer(this, "gotocontent", 4000);
    }

    public void showCardText() {
        cardLabel.setText(getCardTitle(cardIndex));
        cardLabel.startAnimation(fadeIn);
        show(cardLabel);
        cardLabel.bringToFront();
    }

    public void showResultPage() {
        int version = Utils.randomGenerator.nextInt(3) + 1;
        findViewById(R.id.zoom).startAnimation(fadeOut);

        findViewById(R.id.button_top_right).startAnimation(fadeIn);
        findViewById(R.id.button_top_left).startAnimation(fadeIn);

        hide(R.id.button_top_right);
        show(R.id.button_top_left);

        ResultPageIntent.putExtra("cardindex", cardIndex);
        ResultPageIntent.putExtra("versionnumber", version);
        ResultPageIntent.putExtra("type", type);
        startActivity(ResultPageIntent);
        finish();
    }

    public void gotocontent() {
        setContentView(R.layout.screen_resultcontent);
        findViewById(R.id.container).setBackgroundColor(getBackground(type));

        hide(R.id.nav_top_logo);
        hide(R.id.button_top_right);
        hide(R.id.button_top_left);

        remove(R.id.contentBody);
        show(R.id.zoom);
        findViewById(R.id.zoom).setBackgroundColor(Color.BLACK);
        ((ImageView) findViewById(R.id.zoomin)).setImageResource(selectedCardImageId);

        findViewById(R.id.nav_top_logo).startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_in_right));
        show(R.id.nav_top_logo);

        anim2.setDuration(1200);
        findViewById(R.id.zoomin).setAnimation(anim2);
        Utils.setTimer(this, "showResultPage", 900);
    }

    public void hideAllCards() {
        for (int i = 1; i <= NUM_OF_CARD_SPREAD; i++) {
            hide(getID("card" + i, "id"));
        }
    }

    public void setCards() {
        String cardType = getUserPreference("carddeck", "a");
        int cardBg = getID("card_85x142_" + cardType + "_back", "drawable");
        ImageView temp_card;
        for (int i = 1; i <= NUM_OF_CARD_SPREAD; i++) {
            temp_card = findViewById(getID("card" + i, "id"));
            temp_card.setImageResource(cardBg);
            temp_card.bringToFront();
            temp_card.setOnClickListener(aCardOnClickListener);
        }
    }

    public void applyRotation(Boolean isFirstImage, ImageView image1, ImageView image2, float start, float end) {
        AnimationSet mTestAnim = new AnimationSet(true);

        none.setFillAfter(true);
        none.setDuration(1000);

        int location;
        if (image1.getLeft() > (containerWidth / 2)) {
            location = 0 - ((image1.getLeft() - (containerWidth / 2)) + (image1.getWidth() / 1));
        } else if ((image1.getLeft() + image1.getWidth()) < (containerWidth / 2)) {
            location = (containerWidth / 2) - image1.getLeft();
        } else if (image1.getLeft() == (containerWidth / 2)) {
            location = 0 - (image1.getWidth() / 1);
        } else if ((image1.getLeft() + image1.getWidth()) == (containerWidth / 2)) {
            location = (image1.getWidth() / 1);
        } else {
            location = 0;
        }

        TranslateAnimation translate = new TranslateAnimation(TranslateAnimation.ABSOLUTE, 0,
                TranslateAnimation.ABSOLUTE, location,
                TranslateAnimation.ABSOLUTE, 21,
                TranslateAnimation.ABSOLUTE, 21);
        translate.setDuration(1000);

        // Find the center of image
        final float centerX = image1.getWidth() / 2.0f;
        final float centerY = image1.getHeight() / 2.0f;

        scale.setFillAfter(true);
        scale.setStartOffset(500);
        scale.setDuration(500);

        // Create a new 3D rotation with the supplied parameter
        // The animation listener is used to trigger the next animation
        Flip3dAnimation rotation = new Flip3dAnimation(start, end, centerX, centerY);
        rotation.setStartOffset(500);
        rotation.setDuration(500);
        rotation.setFillAfter(true);
        rotation.setInterpolator(new AccelerateInterpolator());
        rotation.setAnimationListener(new DisplayNextView(isFirstImage, image1, image2));

        mTestAnim.addAnimation(none);
        mTestAnim.addAnimation(translate);
        mTestAnim.addAnimation(rotation);
        mTestAnim.addAnimation(scale);
        if (isFirstImage) {
            image1.startAnimation(mTestAnim);
            image1.setVisibility(View.GONE);
            image1.setOnClickListener(null);
            image2.bringToFront();
        } else {
            image2.startAnimation(rotation);
        }

        if (translate.hasEnded()) {
            translate = null;
        }

        if (rotation.hasEnded()) {
            rotation = null;
        }

        mTestAnim = null;
    }

    @Override
    public int getLayout() {
        return R.layout.screen_cardselection;
    }

    @Override
    public void trackPages() {
        FlurryAgent.logEvent("Pick a Card Page: " + type.toString());
        trackScreen("PickACardPage_" + type.toString());
        fireBaseLogEventScreen("PickACardPage_" + type.toString());
    }
}