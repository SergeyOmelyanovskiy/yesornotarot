package app.instantanswertarot.com;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import app.instantanswertarot.com.Const.QUESTION_TYPE;

import com.flurry.android.FlurryAgent;

import java.util.Locale;

public class Activity_ResultContent extends Activity_Base implements OnClickListener {

    private ViewGroup mContainer;
    private TextView cardLabel;
    private ImageView zoomIn, shadow, cardImage;
    private RelativeLayout zoom;
    private int versionNumber, cardIndex;
    private Animation fadeIn, fadeOut, anim, anim2 = null;
    private String resultAnswer, resultDescription;
    private String[] resultTexts;
    private Bundle resultInfo;
    private Const.QUESTION_TYPE type;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initAnim();
        initData();
        initLayout();
        setBottomMenu("home");
    }

    public void initData() {
        Bundle extras = getIntent().getExtras();
        versionNumber = extras.getInt("versionnumber");
        cardIndex = extras.getInt("cardindex");
        type = (QUESTION_TYPE) extras.get("type");
        String cardDeck = getUserPreference("carddeckname", "1001Nights");
        int text_id = getAnswerIdentifier(cardIndex, versionNumber, type.ordinal());
        try {
            resultTexts = getString(text_id).split("_");
            resultAnswer = Html.fromHtml(resultTexts[0]).toString();
            resultDescription = Html.fromHtml(resultTexts[1]).toString();

            resultInfo = new Bundle();
            resultInfo.putString("header", "Yes/No Tarot: " + type.toString());
            resultInfo.putString("cardLabel", getCardTitle(cardIndex));
            resultInfo.putString("Answer", resultAnswer);
            resultInfo.putString("Description", resultDescription);
            resultInfo.putString("cardDeck", cardDeck);
            resultInfo.putInt("cardNumber", cardIndex);
        } catch (Exception e) {
            trackScreen("resultContent_initData" + type.ordinal() + "_" + String.format(Locale.US, "%02d", (cardIndex + 1)) + "_" + versionNumber);
            fireBaseLogEventScreen("resultContent_initData" + type.ordinal() + "_" + String.format(Locale.US, "%02d", (cardIndex + 1)) + "_" + versionNumber);
            startActivity(HomeIntent);
            finish();
        }
    }

    public void zoomIn() {
        remove(R.id.contentBody);
        remove(R.id.ad);
        mContainer.setBackgroundColor(Color.BLACK);
        zoom.startAnimation(fadeIn);
        show(zoom);

        anim.setDuration(1000);
        anim.setRepeatCount(Animation.INFINITE);

        zoomIn.setAnimation(anim);
        show(zoomIn);
        Utils.setTimer(this, "showCardLabel", 900);
    }

    public void showCardLabel() {
        cardLabel.startAnimation(fadeIn);
        cardLabel.setText(getCardTitle(cardIndex));
        show(cardLabel);
    }

    public void zoomOut() {
        anim2.setDuration(1100);
        zoomIn.setAnimation(anim2);
        remove(zoomIn);
        cardLabel.startAnimation(fadeOut);
        remove(cardLabel);

        Utils.setTimer(this, "showZoomOut", 900);
    }

    public void showZoomOut() {
        zoom.startAnimation(fadeOut);
        ResultPageIntent.putExtra("cardindex", cardIndex);
        ResultPageIntent.putExtra("versionnumber", versionNumber);
        ResultPageIntent.putExtra("type", type);
        Utils.interstitial.display(ResultPageIntent, true);
    }

    public int getAnswerIdentifier(int cardindex, int version, int type) {
        char lc_typePrefix = Const.lookUpQuestionTypeByOrdinal(type).toString().charAt(0);
        String ls_id = "YNT" + lc_typePrefix + "_" + String.format(Locale.US, "%02d", (cardindex + 1)) + "_" + version;
        return getID(ls_id, "string");
    }

    public void initAnim() {
        anim = AnimationUtils.loadAnimation(this, R.anim.zoom3);
        anim2 = AnimationUtils.loadAnimation(this, R.anim.zoomoutv2);
        fadeIn = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
        fadeOut = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
    }

    public void initLayout() {
        zoom = findViewById(R.id.zoom);
        zoomIn = findViewById(R.id.zoomin);
        mContainer = findViewById(R.id.container);
        cardImage = findViewById(R.id.cardimage);
        cardLabel = findViewById(R.id.cardlabel);
        shadow = findViewById(R.id.shadow);
        shadow.startAnimation(fadeIn);

        ((TextView) findViewById(R.id.answer)).setText(resultAnswer);
        ((TextView) findViewById(R.id.carddescription)).setText(resultDescription);

        mContainer.setBackgroundResource(getBackground(type));
        zoomIn.setImageResource(getFaceCardId(cardIndex));
        cardImage.setImageResource(getFaceCardId(cardIndex));
        cardImage.setOnClickListener(this);
        zoomIn.setOnClickListener(this);

        findViewById(R.id.button_top_left).setOnClickListener(this);
        Button button_share = findViewById(R.id.button_top_right);
        button_share.setText(R.string.button_share);
        button_share.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int view_id = v.getId();
        if (view_id == R.id.cardimage) {
            zoomIn();
        } else if (view_id == R.id.zoomin) {
            zoomOut();
        } else if (view_id == R.id.button_top_left) {
            Utils.interstitial.display(HomeIntent);
        } else if (view_id == R.id.button_top_right) {
            Utils.setFacebookShare(this, resultInfo);
        }
    }

    @Override
    public void onRestart() {
        super.onRestart();
        showBannerAds();
        remove(zoom);
        mContainer.setBackgroundResource(getBackground(type));
        show(R.id.contentBody);
        show(R.id.ad);
    }

    @Override
    public int getLayout() {
        return R.layout.screen_resultcontent;
    }

    @Override
    public void trackPages() {
        FlurryAgent.logEvent("Result Page " + type.toString());
        trackScreen("ResultPage_" + type.toString());
        fireBaseLogEventScreen("ResultPage_" + type.toString());
    }

    @Override
    public void showBannerAds() {
        loadYielmoAd(R.id.ad, "yieldmoTop.html");
        loadYielmoAd(R.id.adyieldmo, "yieldmoTriple.html");
    }
}    