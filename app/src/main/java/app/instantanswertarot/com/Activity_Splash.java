package app.instantanswertarot.com;

import java.util.Calendar;

import android.os.Bundle;
import android.widget.TextView;

public class Activity_Splash extends Activity_Base {

    public int getLayout() {
        return R.layout.screen_splash;
    }

    public String getTrackingTag() {
        return "Loading Screen";
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        remove(R.id.button_top_left);
        remove(R.id.button_top_right);

        ((TextView) findViewById(R.id.copyright_view)).setText(getString(R.string.copyright, calendar.get(Calendar.YEAR)));
        ((TextView) findViewById(R.id.version)).setText(getString(R.string.app_version, Utils.getVersionNumber(this)));

	    showLoading();
        Utils.setTimer(this, "goToMain", 2000);
    }

    public void goToMain() {
        hideLoading();
        startActivity(HomeIntent);
        finish();
    }
}