package app.instantanswertarot.com;

class Const {

    enum QUESTION_TYPE {GENERAL, LOVE, CAREER, MONEY}

    enum MARKET_TYPE {ANDRO, AMAZONANDRO}

    static QUESTION_TYPE lookUpQuestionTypeByOrdinal(int i) {
        return QUESTION_TYPE.values()[i];
    }

    //**************************** API KEYS ***************************/
    static final String FBAppID = "285111089360";
    static final String AdMobMediationKey = "54a7a2d0a3944711";
    //static final String AdMobInterstitialMediationKey = "ca-app-pub-6667975737036134/7790625705"; //ada5e56d42604e70";
    static final String AdMobInterstitialKey = "ca-app-pub-6667975737036134/7790625705"; //"a14cb7a2a41baca"; //"a14b60cca1cc2e4";
    static final String AdMobInterstitialTestKey = "ca-app-pub-6667975737036134/7954742506";
    //private static final String GoogleAnalyticsKey = "UA-338877-36";
    static final String QuantcastKey = "1bluj0e4vfqh5oza-wnsv46psvj593qa7";
    static final String FlurryKey = "WDG5IJ8V37TI7NJIVQBK";
    static final String APP_PREF = "Preferences";
    static final String CS_LAST_INTERSTITIAL_SHOWED = "interstitial_last_showed";
    static final int CI_INTERSTITIAL_INTERVAL_IN_MIN = 4 * 60;

    static final long SEC = 1000L;
    static final long MIN = 60 * 1000L;
    static final long HOUR = 60 * 60 * 1000L;
    static final long DAY = 24 * 60 * 60 * 1000L;

    //**************************** SHARED VARIABLES *********************/
    static final MARKET_TYPE l_markettype = MARKET_TYPE.ANDRO;

    static final int[] menuTextId = {R.string.email, R.string.cancel};
    static final int[] menuIconId = {R.drawable.email, R.drawable.cancel};
}