package app.instantanswertarot.com;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.SimpleAdapter;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.yieldmo.sdk.YMException;
import com.yieldmo.sdk.YMPlacementListener;
import com.yieldmo.sdk.YMPlacementView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Utils {

    static final Random randomGenerator = new Random();
    static final Handler mHandler = new Handler();
    static final myInterstitial interstitial = new myInterstitial();

    private static String generateURL(String link) {
        return generateURL(link, null);
    }

    static String generateURL(String link, Bundle params) {
        StringBuilder Url = new StringBuilder(link);
        if (!Url.toString().contains("://"))
            Url.insert(0, ((Boolean) false ? "https://" : "http://"));
        if (params != null && !params.isEmpty()) {
            for (String key : params.keySet()) {
                Url.append(Url.toString().contains("?") ? "&" : "?");
                Url.append(key).append("=").append(params.getString(key));
            }
        }

        return Url.toString();
    }

    static String getServerLink(Activity a, String link) {
        return a.getResources().getString(BuildConfig.DEBUG ? R.string.url_domain_test : R.string.url_domain_prod, link);
    }

    static String getMarketLink(Activity a) {
        return generateURL(a.getResources().getString(Const.l_markettype == Const.MARKET_TYPE.AMAZONANDRO ? R.string.url_amazonmarket : R.string.url_playstore, a.getPackageName()));
    }

    static int getID(Activity activity, String l_idString, String l_type) {
        int resid;
        try {    // l_type -> array, drawable, string,etc
            Resources res = activity.getResources();
            resid = res.getIdentifier(l_idString, l_type, activity.getPackageName());
            debugLog("getID", "R." + l_type + "." + l_idString, false);
        } catch (Exception e) {
            debugLog("getID", e.toString(), false);
            if ("drawable".equals(l_type)) {
                resid = R.drawable.card_c_1;
            } else {
                resid = R.string.card_1;
            }
        }

        return resid;
    }

    static String getVersionNumber(Activity a) {
        String version;
        try {
            version = a.getApplicationContext().getPackageManager().getPackageInfo(a.getPackageName(), 0).versionName;
        } catch (Exception e) {
            return "v2.7.3";
        }
        return version + (BuildConfig.DEBUG ? "-dev" : "");
    }

//    TODO: Doesn't use
//    public static void alert(String title, String object, Context c) {
//        new AlertDialog.Builder(c)
//                .setMessage(object)
//                .setTitle(title)
//                .setCancelable(true)
//                .setNeutralButton(android.R.string.ok, null)
//                .show();
//    }

//    TODO: Doesn't use
//    public static String htmlEntityEncode(String s) {
//        StringBuilder buf = new StringBuilder(s.length());
//        String ls_result;
//        for (int i = 0; i < s.length(); i++) {
//            char c = s.charAt(i);
//            if (c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z' || c >= '0' && c <= '9') {
//                buf.append(c);
//            } else {
//                buf.append("&#").append((int) c).append(";");
//            }
//        }
//
//        ls_result = buf.toString();
//        return ls_result;
//    }

    private static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return dp * (metrics.densityDpi / 160f);
    }

    static void reloadAd(Activity a, final int wrapperID) {
        final LinearLayout layout = a.findViewById(wrapperID);
        if (layout.getChildAt(0) != null && layout.getChildAt(0) instanceof WebView) {
            ((WebView) layout.getChildAt(0)).reload();
        }
    }

    static void showAd(final Activity a, final int wrapperID, final String adScript) {
        showAdWithSDK(a, wrapperID, adScript);
		
		/*
		 final LinearLayout layout = (LinearLayout)a.findViewById(wrapperID); 
		 layout.removeAllViews();
		 int dip_5 =  (int)convertDpToPixel(5, a);
		 int dip_10 =  (int)convertDpToPixel(10, a);
		 final LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) layout.getLayoutParams();
		 if (adscript.indexOf("yieldmoBottom") >= 0)   layoutParams.setMargins(0, 0, 0, 0); 
		 else if (adscript.indexOf("yieldmoTop") >= 0)   layoutParams.setMargins(dip_10, dip_5, dip_10, 0); 
		 else if (adscript.indexOf("yieldmoTriple") >= 0) layoutParams.setMargins(dip_10, dip_5, dip_10, dip_10);
		 
		 
		 WebView webview = new WebView(a) {
			 @Override
			 public void onSizeChanged(int w, int h, int ow, int oh) {
				 if (getUrl() != null && getContentHeight() >= 0) {
					 if (getUrl().indexOf("yieldmoBottom") >= 0)   layoutParams.height = (int)convertDpToPixel(95, a);
					 else if (getUrl().indexOf("yieldmoTop") >= 0)   layoutParams.height = (int)convertDpToPixel(65, a); 
					 else if (getUrl().indexOf("yieldmoTriple") >= 0) layoutParams.height = (int)convertDpToPixel(280, a);
					 layout.requestLayout();// layout.recomputeViewAttributes(this);
				 } 
				 super.onSizeChanged(w, h, ow, oh);
			 } 	
			 
		 }; 
	
		 webview.setWebViewClient(new WebViewClient() {
		    @Override
		    public boolean shouldOverrideUrlLoading(WebView view, String url) {
		        if (url != null && url.startsWith("http://")) {
		            view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
		            return true;
		        } else {
		            return false;
		        }
		    }
		 });
		 webview.getSettings().setJavaScriptEnabled(true);
		 if (!isDeviceConnected(a)) webview.loadData("<body style=\"margin-top:-10px\"></body>", "text/html", "utf-8");
		 else webview.loadUrl("file:///android_asset/"+adscript);
			
		 webview.setVerticalScrollBarEnabled(false);
		 webview.setBackgroundColor(Color.TRANSPARENT);
		 webview.setBackgroundResource(R.drawable.gradientv4); 	 
		 layout.addView(webview);
		 */
    }

    private static void showAdWithSDK(final Activity a, final int wrapperID, final String adScript) {
        final LinearLayout layout = a.findViewById(wrapperID);
        layout.removeAllViews();
        int dip_5 = (int) convertDpToPixel(5, a);
        int dip_10 = (int) convertDpToPixel(10, a);
        final LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) layout.getLayoutParams();
        if (adScript.indexOf("yieldmoBottom") >= 0) {
            layoutParams.setMargins(0, 0, 0, 0);
        } else if (adScript.indexOf("yieldmoTop") >= 0) {
            layoutParams.setMargins(dip_5, dip_5, dip_5, 0);
        } else if (adScript.indexOf("yieldmoTriple") >= 0) {
            layoutParams.setMargins(dip_5, 0, dip_5, dip_10);
        }

        String placementId = "";
        YMPlacementView ymView;

        if (adScript.indexOf("yieldmoTriple") >= 0) {
            placementId = "804089393077493684"; //id for IOS since Android Id: "819539016340413504", not working
        } else if (adScript.indexOf("yieldmoBottom") >= 0) {
            placementId = "819540114098822209";
        } else if (adScript.indexOf("yieldmoTop") >= 0) {
            placementId = "819538325454658623";
        }

        ymView = new YMPlacementView.Builder(a).placementId(placementId).listener(new YMPlacementListener() {
            @Override
            public void adDisplayFailed(YMException e) {
                Log.e("kdkdkd", "Yieldmo Ad Failed");
                if (layout.getVisibility() == View.VISIBLE) {
                    layout.setVisibility(View.GONE);
                }
            }

            @Override
            public void adDisplayed() {
                Log.v("kdkdkd", "Yieldmo Ad Displayed");
                if (layout.getVisibility() == View.GONE) {
                    layout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void adLeavesApplication() {
                Log.v("kdkdkd", "Yieldmo Ad Leaves Application");
            }

            @Override
            public void adClicked() {
                Log.v("kdkdkd", "Yieldmo Ad Close Clicked");
            }
        }).build();
        layout.addView(ymView, layoutParams);
    }


    static boolean isDeviceConnected(Context c) {
        ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = null;
        if (cm != null) {
            activeNetwork = cm.getActiveNetworkInfo();
        }

        boolean isConnected = false;
        if (activeNetwork != null) {
            isConnected = activeNetwork.isConnectedOrConnecting();
        }

        return isConnected;
    }

    static void setFacebookShare(final Activity a, final Bundle facebookParams) {
        List<Map<String, Object>> list = new ArrayList<>();
        String[] menuObject = new String[]{"text", "icon"};
        int[] menuObjectLayoutId = new int[]{R.id.text, R.id.icon};
        int numberOfItems = Const.menuTextId.length;
        for (int i = 0; i < numberOfItems; i++) {
            Map<String, Object> adapterMap = new HashMap<>();
            adapterMap.put(menuObject[0], a.getResources().getString(Const.menuTextId[i]));
            adapterMap.put(menuObject[1], Const.menuIconId[i]);
            list.add(adapterMap);
        }

        new AlertDialog.Builder(a).setAdapter(new SimpleAdapter(a, list, R.layout.include_list_item, menuObject, menuObjectLayoutId), getShareListClickListener(a, facebookParams)).show();
    }

    private static OnClickListener getShareListClickListener(final Activity a, final Bundle Params) {
        return new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int menuPos) {
                if (menuPos == 0) {
                    Activity_Info.emailFriend(Params.getString("header") + " by horoscope.com", Params.getString("cardLabel") + ": " + Params.getString("Answer") + "\n" + Params.getString("Description"), a);
                } else if (menuPos == 1) {
                    a.setResult(Activity.RESULT_CANCELED, null);
                }
            }
        };
    }

//    TODO: Doesn't use
//    public static void debugLog(String LOG_TAG, String message) {
//        debugLog(LOG_TAG, message, false);
//    }

    private static void debugLog(String LOG_TAG, String message, Boolean byPassIsDebug) {
        if (BuildConfig.DEBUG || byPassIsDebug) {
            Log.d(LOG_TAG, message);
        }
    }

    static void setTimer(final Activity a, final String function_name, int millisec) {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                new myFunction(a, function_name).execute();
            }
        }, millisec);
    }

    private static String getAdMobInterstitialKey() {
        return BuildConfig.DEBUG ? Const.AdMobInterstitialTestKey : Const.AdMobInterstitialKey;
    }

    public static class myInterstitial {
        Activity myActivity;
        InterstitialAd interstitialAd;
        Intent myIntent;
        Boolean finishActivity;

        void init(Activity activity) {
            if (myActivity == activity) return;
            myActivity = activity;

            AdRequest adRequest = new AdRequest.Builder()
                    //.addTestDevice("70F138DD54B7AEA1F1660E73B297B215")
                    //.addTestDevice("33C59FFC686A80974BD7E06CAB8D9210")
                    .build();

            if (!interstitialIsEnabled()) {
                return;
            }

            interstitialAd = new InterstitialAd(activity);
            interstitialAd.setAdUnitId(getAdMobInterstitialKey());
            interstitialAd.loadAd(adRequest);
            interstitialAd.setAdListener(InterstitialAdListener);
        }

        boolean adIsReady() {
            return interstitialAd != null && interstitialAd.isLoaded();
        }

        boolean interstitialIsEnabled() {
            long lastDisplayedTime = myActivity.getSharedPreferences(Const.APP_PREF, 0).getLong(Const.CS_LAST_INTERSTITIAL_SHOWED, 0L);
            return (System.currentTimeMillis() - lastDisplayedTime) / Const.MIN >= Const.CI_INTERSTITIAL_INTERVAL_IN_MIN;
        }

        void display(Intent intent) {
            display(intent, false);
        }

        void display(Intent intent, boolean finish) {
            myIntent = intent;
            finishActivity = finish;
            if (adIsReady()) {
                interstitialAd.show();
                myActivity.getSharedPreferences(Const.APP_PREF, 0).edit().putLong(Const.CS_LAST_INTERSTITIAL_SHOWED, System.currentTimeMillis()).apply();
            } else goToContent();
        }

        void goToContent() {
            if (myIntent != null && myActivity != null) {
                myActivity.startActivity(myIntent);
                if (finishActivity) {
                    myActivity.finish();
                }
            }
        }
    }

    private static AdListener InterstitialAdListener = new AdListener() {

        @Override
        public void onAdFailedToLoad(int errorCode) {

        }

        @Override
        public void onAdOpened() {

        }
    };

    public static class myFunction {
        private Activity mActivity;
        String mFunctionName;
        Class[] mFunctionArgTypes;
        Object[] mFunctionArgs;

        myFunction(Activity a, String name) {
            mActivity = a;
            mFunctionName = name;
        }

        public myFunction(Activity a, String name, Class[] parameterTypes, Object[] args) {
            this(a, name);
            mFunctionArgTypes = parameterTypes;
            mFunctionArgs = args;
        }

        void execute() {
            try {
                mActivity.getClass().getMethod(mFunctionName, mFunctionArgTypes).invoke(mActivity, mFunctionArgs);
            } catch (Exception e) {
                FlurryAgent.onError("Error", "myFunction.execute():", e.toString());
            }
        }
    }
}